#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <sstream>
#include <regex>
#include "avahi-finder.h"
#include "csv.h"

static const char* CMD = "/usr/bin/avahi-browse _ndi._tcp -p -t --resolve";

static std::stringstream exec(const char* cmd);
static const std::string getEnvVariable(std::string const& key);

std::vector<std::pair<std::string,std::string>> Avahi_Finder::listSources() {
  std::vector<std::pair<std::string,std::string>> result;
  std::vector<std::pair<std::string,std::string>> preferredResult;
  std::stringstream output = exec(CMD);

  csv::CSVFormat format;
  format.delimiter(';').no_header();

  csv::CSVReader reader(output, format);
  const std::string preferredInterface = getEnvVariable("PREFERRED_NIC");

  for (csv::CSVRow& row: reader) {
    std::string status = row[0].get<>();
    if (status != "=") {
      continue;
    }
    std::string iface = row[1].get<>();
    std::string protocol = row[2].get<>();
    if (protocol != "IPv4") {
      continue;
    }
    std::string ndiName = row[3].get<>();
    ndiName = std::regex_replace(ndiName, std::regex("\\\\032"), " ");
    ndiName = std::regex_replace(ndiName, std::regex("\\\\040"), "(");
    ndiName = std::regex_replace(ndiName, std::regex("\\\\041"), ")");
    std::string address = row[7].get<>();
    std::string port = row[8].get<>();
    std::string url = address + ":" + port;
    bool exists = false;

    for (std::pair<std::string,std::string> source : result) {
      if (source.first.compare(ndiName) == 0) {
        exists = true;
        break;
      }
    }

    if (!exists) {
      result.push_back({ndiName, url});
    }

    if (preferredInterface == iface) {
      std::cerr << "Found " << ndiName << " on preferred interface " << preferredInterface << std::endl;
      preferredResult.push_back({ndiName, url});
    }
  }

  if (preferredResult.size() > 0) {
    return preferredResult;

  } else {
    return result;
  }
}

static const std::string getEnvVariable(std::string const& key) {
    char * val;
    val = getenv(key.c_str());
    std::string retval = "";
    if (val != NULL) {
        retval = val;
    }

    return retval;
}

static std::stringstream exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::stringstream result;

    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result << buffer.data();
    }
    return result;
}
