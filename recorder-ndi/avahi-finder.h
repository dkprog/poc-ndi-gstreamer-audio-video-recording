#pragma once
#include <vector>

namespace Avahi_Finder {
  std::vector<std::pair<std::string,std::string>> listSources();
}
