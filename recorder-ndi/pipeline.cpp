#include <iostream>
#include <csignal>
#include "globals.h"
#include "pipeline.h"

using namespace std;

Pipeline::Fields Pipeline::pipeline;

GstBus *bus;

void signalHandler(int signum);

void Pipeline::create() {
  signal(SIGINT, signalHandler);

  pipeline.pipeline = gst_pipeline_new(PACKAGE);

  pipeline.video_appsrc = gst_element_factory_make(
    "appsrc",
    "video_appsrc"
  );

  pipeline.videoconvert = gst_element_factory_make(
    "videoconvert",
    "videoconvert"
  );

  pipeline.x264enc = gst_element_factory_make(
    "x264enc",
    "x264enc"
  );  

  pipeline.audio_appsrc = gst_element_factory_make(
    "appsrc",
    "audio_appsrc"
  );

  pipeline.audioconvert = gst_element_factory_make(
    "audioconvert",
    "audioconvert"
  );

  pipeline.audioresample = gst_element_factory_make(
    "audioresample",
    "audioresample"
  );

  pipeline.opusenc = gst_element_factory_make(
    "opusenc",
    "opusenc"
  );  

  pipeline.mp4mux = gst_element_factory_make(
    "mp4mux",
    "mp4mux"
  );

  pipeline.filesink = gst_element_factory_make(
    "filesink",
    "filesink"
  );

  pipeline.playing = false;
  pipeline.terminate = false;

  if (
    !pipeline.pipeline ||
    !pipeline.video_appsrc ||
    !pipeline.videoconvert ||
    !pipeline.x264enc ||
    !pipeline.audio_appsrc ||
    !pipeline.audioconvert ||
    !pipeline.audioresample ||
    !pipeline.opusenc ||
    !pipeline.mp4mux ||
    !pipeline.filesink
  ) {
    cerr << "Error: Not all gstreamer elements could be created." << endl;
    exit(EXIT_FAILURE_GSTREAMER);
  }

  gst_bin_add_many(
    GST_BIN(pipeline.pipeline),
    pipeline.video_appsrc,
    pipeline.videoconvert,
    pipeline.x264enc,
    pipeline.audio_appsrc,
    pipeline.audioconvert,
    pipeline.audioresample,
    pipeline.opusenc,
    pipeline.mp4mux,
    pipeline.filesink,
    NULL
  );

  if (
    !gst_element_link_many(
      pipeline.video_appsrc,
      pipeline.videoconvert,
      pipeline.x264enc,
      pipeline.mp4mux,
      pipeline.filesink,
      NULL
    )
  ) {
    cerr << "Error: Not all video elements could be linked." << endl;
    exit(EXIT_FAILURE_GSTREAMER);
  }

  if (
    !gst_element_link_many(
      pipeline.audio_appsrc,
      pipeline.audioconvert,
      pipeline.audioresample,
      pipeline.opusenc,
      pipeline.mp4mux,
      NULL
    )
  ) {
    cerr << "Error: Not all audio elements could be linked." << endl;
    exit(EXIT_FAILURE_GSTREAMER);
  }
  
  g_object_set(
    pipeline.video_appsrc,
    "is-live", TRUE,
    "stream-type", 0,
    "format", GST_FORMAT_TIME,
    "do-timestamp", TRUE,
    NULL
  );

  g_object_set(
    pipeline.audio_appsrc,
    "is-live", TRUE,
    "stream-type", 0,
    "format", GST_FORMAT_TIME,
    NULL
  );

  g_object_set(
    pipeline.filesink,
    "location", "video.mp4",
    NULL
  );  
}

void Pipeline::process() {

  GstMessage *msg;

  bus = gst_element_get_bus(pipeline.pipeline);
  do {
    msg = gst_bus_timed_pop_filtered(
      bus,
      GST_CLOCK_TIME_NONE,
      (GstMessageType)(
        GST_MESSAGE_STATE_CHANGED |
        GST_MESSAGE_ERROR |
        GST_MESSAGE_EOS
      )
    );

    if (msg != NULL) {
      GError *err;
      gchar *debug_info;

      switch (GST_MESSAGE_TYPE(msg)) {
        case GST_MESSAGE_ERROR:
          gst_message_parse_error(msg, &err, &debug_info);

          cerr << "Error received from element " <<
            GST_OBJECT_NAME(msg->src) << 
            ": " <<
            err->message <<
            endl;

          cerr << "Debugging information: " <<
            (debug_info ? debug_info : "none") <<
            endl;

          g_clear_error(&err);
          g_free(debug_info);
          pipeline.terminate = TRUE;
          break;
        case GST_MESSAGE_EOS:
          cerr << "End-Of-Stream (EOS) reached." << endl;

          pipeline.terminate = TRUE;
          break;
        case GST_MESSAGE_STATE_CHANGED:
          if (GST_MESSAGE_SRC(msg) == GST_OBJECT(pipeline.pipeline)) {
            GstState old_state, new_state, pending_state;
            gst_message_parse_state_changed(
              msg, 
              &old_state, 
              &new_state, 
              &pending_state
            );
            cerr << "Pipeline state changed from " <<
              gst_element_state_get_name(old_state) <<
              " to " <<
              gst_element_state_get_name(new_state) <<
              endl;

            if (
              g_str_equal(
                gst_element_state_get_name(new_state),
                "PLAYING"
              )
            ) {
              GST_DEBUG_BIN_TO_DOT_FILE(
                GST_BIN(pipeline.pipeline),
                GST_DEBUG_GRAPH_SHOW_ALL,
                "pipeline"
              );
            }
          }
          break;
        default:
          cerr << "Unexpected message received." << endl;
          break;
      }
      gst_message_unref(msg);
    }
  } while (!pipeline.terminate);
  
  gst_object_unref(bus);
  gst_element_set_state(pipeline.pipeline, GST_STATE_READY);
  gst_object_unref(pipeline.pipeline);
}

void signalHandler(int signum) {
  using namespace Pipeline;

  cout << "Interrupt signal (" << signum << ") received." << endl;
  gst_element_send_event(pipeline.pipeline, gst_event_new_eos());
}