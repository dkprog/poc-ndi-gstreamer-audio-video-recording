#include <iostream>
#include "globals.h"
#include "pipeline.h"
#include "ndi-receiver.h"

using namespace std;

int main(int argc, char *argv[]) {
  cerr << PACKAGE << endl;
  cerr << endl;

  gst_init(&argc, &argv);

  Pipeline::create();

  NDI_Receiver::start();

  Pipeline::process();

  NDI_Receiver::stop();

  cerr << "Goodbye." << endl;
  
  return EXIT_SUCCESS;
}
