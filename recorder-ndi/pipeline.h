#pragma once
#include <gst/gst.h>
#include <gst/audio/audio.h>

namespace Pipeline {
  typedef struct {
    GstElement *pipeline;
    GstElement *video_appsrc;
    GstElement *videoconvert;
    GstElement *x264enc;
    GstElement *audio_appsrc;
    GstElement *audioconvert;
    GstElement *audioresample;
    GstElement *opusenc;

    GstElement *mp4mux;
    GstElement *filesink;
    bool playing;
    bool terminate;
  } Fields;

  extern Fields pipeline;

  void create();
  void process();
}
