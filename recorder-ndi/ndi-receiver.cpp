#include <iostream>
#include <Processing.NDI.Lib.h>
#include <pthread.h>
#include "globals.h"
#include "ndi-receiver.h"
#include "pipeline.h"
#include "avahi-finder.h"

using namespace std;

static void* thread_proc(
  void *p_data
);

static pthread_t thread;
static NDIlib_recv_instance_t pNDI_recv;

void NDI_Receiver::start() {
  static string ndiName, ndiUri;

  pNDI_recv = NDIlib_recv_create_v3();
  if (!pNDI_recv) {
    cerr << "Could not create a NDI recv instance." << endl;
    exit(EXIT_FAILURE_NDILIB);
  }

  const NDIlib_source_t *p_source = NULL;

  cerr << "Looking for NDI sources..." << endl;

  for (pair<string, string> & source : Avahi_Finder::listSources()) {
    cerr << "Found NDI device: " <<
      source.first << 
      " " <<
      source.second << endl;

    ndiName = source.first;
    ndiUri = source.second;
    
    // TODO: select with a criteria
    p_source = new NDIlib_source_t(ndiName.c_str(), ndiUri.c_str());
    break;
  }

  if (!p_source) {
    cerr << "Error: No NDI source found." << endl;
    exit(EXIT_FAILURE_NDILIB);
  }

  NDIlib_recv_connect(pNDI_recv, p_source);

  delete[] p_source;

  if (pthread_create(&thread, NULL, thread_proc, &Pipeline::pipeline)) {
    cerr << "Could not create thread." << endl;
    exit(EXIT_FAILURE);
  }
}

void NDI_Receiver::stop() {
  NDIlib_recv_destroy(pNDI_recv);
  NDIlib_destroy();
}

static void* thread_proc(
  void *p_data
) {
  Pipeline::Fields *ppipeline = (Pipeline::Fields *)p_data;
  GstCaps *video_caps = NULL;
  GstCaps *audio_caps = NULL;

  cerr << "Receiving frames..." << endl;

  while (!ppipeline->terminate) {
    NDIlib_video_frame_v2_t video_frame;
    NDIlib_audio_frame_v2_t audio_frame;
    NDIlib_frame_type_e frame_type;

    frame_type = NDIlib_recv_capture_v2(
      pNDI_recv, 
      &video_frame, 
      &audio_frame,
      nullptr,
      5000
    );

    bool is_video = frame_type == NDIlib_frame_type_video &&
                      video_frame.FourCC == NDIlib_FourCC_video_type_UYVY;

    bool is_audio = frame_type == NDIlib_frame_type_audio;

    if (is_video && !video_caps) {
      video_caps = gst_caps_new_simple(
        "video/x-raw",
        "format", G_TYPE_STRING, "UYVY",
        "width", G_TYPE_INT, video_frame.xres,
        "height", G_TYPE_INT, video_frame.yres,
        NULL
      );

      g_object_set(
        ppipeline->video_appsrc,
        "caps", video_caps,
        NULL
      );
    }
    else if (is_audio && !audio_caps) {
      audio_caps = gst_caps_new_simple(
        "audio/x-raw",
        "format", G_TYPE_STRING, "F32LE",
        "channels", G_TYPE_INT, audio_frame.no_channels,
        "rate", G_TYPE_INT, audio_frame.sample_rate,
        "layout", G_TYPE_STRING, "non-interleaved",
        NULL
      );

      g_object_set(
        ppipeline->audio_appsrc,
        "caps", audio_caps,
        NULL
      );
    }

    if (video_caps && audio_caps && !ppipeline->playing) {
      GstStateChangeReturn ret;

      ret = gst_element_set_state(
        ppipeline->pipeline,
        GST_STATE_PLAYING
      );

      if (ret == GST_STATE_CHANGE_FAILURE) {
        cerr << 
          "Error: Unable to set the pipeline to the playing state." <<
          endl;
        ppipeline->terminate = true;
        continue;
      }
      else {
        ppipeline->playing = true;
        cerr << "Playing the pipeline." << endl;
      }
    }

    if (is_video && ppipeline->playing) {
#ifdef DEBUG      
      cerr << "Frame type: NDIlib_frame_type_video" << endl;
      cerr << "Format: NDIlib_FourCC_video_type_UYVY" << endl;

      cerr << "Width: " << 
        video_frame.xres <<
        " Height: " <<
        video_frame.yres <<
        endl <<
        endl;
#endif

      GstFlowReturn ret;
      guint size = video_frame.yres * video_frame.xres * 2;
      GstBuffer *buffer = gst_buffer_new_memdup(
        video_frame.p_data,
        size
      );
      g_signal_emit_by_name(
        ppipeline->video_appsrc,
        "push-buffer",
        buffer,
        &ret
      );

      gst_buffer_unref(buffer);

      if (ret != GST_FLOW_OK) {
        ppipeline->terminate = true;
      }
    }
    if (is_audio && ppipeline->playing) {
#ifdef DEBUG
      cerr << "Frame type: NDIlib_frame_type_audio" << endl;
      cerr << "Format: NDIlib_FourCC_audio_type_FLTP" << endl;
      cerr << "Audio channels: " << audio_frame.no_channels << endl;
      cerr << "Audio rate: " << audio_frame.sample_rate << endl;
      cerr << "Audio samples: " << audio_frame.no_samples << endl;
      cerr << "Audio channel stride in bytes: " <<
        audio_frame.channel_stride_in_bytes <<
        endl << endl;
#endif

      GstFlowReturn ret;
      guint size = 
        sizeof(float) * audio_frame.no_samples * audio_frame.no_channels;
      GstBuffer *buffer = gst_buffer_new_memdup(
        audio_frame.p_data,
        size
      );
      GstAudioInfo *audio_info = gst_audio_info_new_from_caps(audio_caps);

      gst_buffer_add_audio_meta(
        buffer,
        audio_info,
        audio_frame.no_samples,
        NULL
      );

      g_signal_emit_by_name(
        ppipeline->audio_appsrc,
        "push-buffer",
        buffer,
        &ret
      );

      gst_buffer_unref(buffer);

      if (ret != GST_FLOW_OK) {
        ppipeline->terminate = true;
      }
    }    

    NDIlib_recv_free_video_v2(
      pNDI_recv, 
      &video_frame
    );

    NDIlib_recv_free_audio_v2(
      pNDI_recv,
      &audio_frame
    );
  }

  return NULL;  
}
