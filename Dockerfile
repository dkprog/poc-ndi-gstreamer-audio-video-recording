FROM ubuntu:22.04 AS base
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -qq install -y \
  avahi-daemon \
  avahi-utils \
  gstreamer1.0-plugins-base \
  gstreamer1.0-plugins-good \
  gstreamer1.0-plugins-bad \
  gstreamer1.0-plugins-ugly \
  gstreamer1.0-libav \
  gstreamer1.0-tools \
  gstreamer1.0-x \
  gstreamer1.0-alsa \
  gstreamer1.0-gl \
  gstreamer1.0-gtk3 \
  gstreamer1.0-qt5 \
  gstreamer1.0-pulseaudio
ADD lib/x86_64-linux-gnu /app/lib/x86_64-linux-gnu/
ENV LD_LIBRARY_PATH="/app/lib/x86_64-linux-gnu/"
RUN ldconfig -v
WORKDIR /app

FROM base AS build
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -qq install -y \
  build-essential \
  gdb \
  pkg-config \
  libgstreamer1.0-dev \
	libgstreamer-plugins-base1.0-dev \
	libgstreamer-plugins-bad1.0-dev
ADD include /app/include
ADD recorder-ndi /app/recorder-ndi
RUN cd /app/recorder-ndi; make

FROM build AS dev
ARG UNAME=developer
ARG UID=1000
ARG GID=1000
ENV UNAME=$UNAME
RUN groupadd -g $GID -o $UNAME && \
	useradd -m -u $UID -g $GID -o -s /bin/bash $UNAME
ADD . /app
USER root
ADD startup-dev.sh /app/startup-dev.sh
ENTRYPOINT ["/app/startup-dev.sh"]